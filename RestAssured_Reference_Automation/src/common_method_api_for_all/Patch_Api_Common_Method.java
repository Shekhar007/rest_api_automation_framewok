package common_method_api_for_all;
import static io.restassured.RestAssured.given;

public class Patch_Api_Common_Method {


     public static int patch_statusCode(String requestBody, String endpoint) 
     { 
    	int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
        .then().extract().statusCode(); return statusCode;
     }
  
    public static String patch_responseBody(String requestBody, String endpoint)
    {
      String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
      .then().extract().response().asString(); return responseBody;
    
    }
}
