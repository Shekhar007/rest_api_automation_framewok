package common_method_api_for_all;

import static io.restassured.RestAssured.given;

import java.net.URI;

public class Put_Api_Common_Method
{
     public static int put_statusCode(String requestBody, String endpoint)
    {
        int statusCode = given().header("Content-Type","application/json").body(requestBody).when().put(endpoint)
        .then().extract().statusCode(); return statusCode; 
     }
  
  public static String put_responseBody(String requestBody, String endpoint)
    { 
	String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
     .then().extract().response().asString(); return responseBody;
  
    } 
}