package rest_assured_ref;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Get_Reference {
	 public static void main(String[] args) {
		//declare base uri
		 RestAssured.baseURI ="https://reqres.in/";
		 String id="2";
		 String email="janet.weaver@reqres.in";
		 
		String ResponseBody = given().header("Content-Type", "application/json").when().get("/api/users/2")
		 .then().extract().response().asString();
		System.out.println(ResponseBody);
		
		//create a json object to prase the response
		JsonPath jspres=new JsonPath(ResponseBody);
		String jid=jspres.get(ResponseBody);
		System.out.println(jid);
	//	int rid =data.indexOf(0);
	//	System.out.println(rid);
		
		//validation responsebody
	//	Assert.assertEquals(resid,id);
		//Assert.assertEquals(email, resemail);
	}
 }
