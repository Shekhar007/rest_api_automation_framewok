package common_method_api_for_all;

import static io.restassured.RestAssured.given;

public class Get_Api_Common_Method

{
	public static int get_statusCode(String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").when().get(endpoint).
				then().extract().statusCode();
		return statusCode;
	}

	public static String get_responseBody(String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.response().asString();
		return responseBody;
	}
}